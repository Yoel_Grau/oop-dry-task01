import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

class StudentTest {

    private Student student;
    private GradeCalculator gradeCalculator;

    @BeforeEach
    void setUp() {
        gradeCalculator = new SimpleGradeCalculator();
        student = new Student(1, "Test Student", gradeCalculator);
    }

    @Test
    void calculateAverageWithNoCourses() {
        double average = student.calculateAverage();
        assertEquals(0.0, average);
    }

    @Test
    void calculateAverageWithSingleCourse() {
        student.addSubject(new Course("Math", 90.0));
        double average = student.calculateAverage();
        assertEquals(90.0, average);
    }

    @Test
    void calculateAverageWithMultipleCourses() {
        student.addSubject(new Course("Math", 90.0));
        student.addSubject(new Course("Science", 80.0));
        student.addSubject(new Course("English", 85.0));
        double average = student.calculateAverage();
        assertEquals((90.0 + 80.0 + 85.0) / 3, average);
    }

    @Test
    void testGetName() {
        assertEquals("Test Student", student.getName());
    }

    @Test
    void testGetId() {
        assertEquals(1, student.getId());
    }

    @Test
    void testGetSubjects() {
        student.addSubject(new Course("Math", 90.0));
        student.addSubject(new Course("Science", 80.0));
        List<Course> courses = student.getSubjects();
        assertEquals(2, courses.size());
        assertEquals("Math", courses.get(0).getName());
        assertEquals(90.0, courses.get(0).getScore());
        assertEquals("Science", courses.get(1).getName());
        assertEquals(80.0, courses.get(1).getScore());
    }
}
