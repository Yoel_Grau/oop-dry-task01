import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CourseTest {

    @Test
    void testGetName() {
        Course course = new Course("Matemáticas", 95.0);
        assertEquals("Matemáticas", course.getName());
    }

    @Test
    void testGetScore() {
        Course course = new Course("Matemáticas", 95.0);
        assertEquals(95.0, course.getScore(), 0.01);
    }

    @Test
    void testGetNameWithDifferentValues() {
        Course course = new Course("Ciencias", 88.5);
        assertEquals("Ciencias", course.getName());
    }

    @Test
    void testGetScoreWithDifferentValues() {
        Course course = new Course("Ciencias", 88.5);
        assertEquals(88.5, course.getScore(), 0.01);
    }

    @Test
    void testGetScoreWithNegativeValue() {
        Course course = new Course("Arte", -10.0);
        assertEquals(-10.0, course.getScore(), 0.01);
    }

    @Test
    void testGetNameWithEmptyString() {
        Course course = new Course("", 100.0);
        assertEquals("", course.getName());
    }
}

