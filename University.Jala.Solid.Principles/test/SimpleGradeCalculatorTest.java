import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SimpleGradeCalculatorTest {

    @Test
    void calculateAverageWithEmptyCourseList() {
        GradeCalculator calculator = new SimpleGradeCalculator();
        List<Course> courses = new ArrayList<>();

        double average = calculator.calculateAverage(courses);

        assertEquals(0.0, average);
    }

    @Test
    void calculateAverageWithSingleCourse() {
        GradeCalculator calculator = new SimpleGradeCalculator();
        List<Course> courses = new ArrayList<>();
        courses.add(new Course("Matemáticas", 95.0));

        double average = calculator.calculateAverage(courses);

        assertEquals(95.0, average);
    }

    @Test
    void calculateAverageWithMultipleCourses() {
        GradeCalculator calculator = new SimpleGradeCalculator();
        List<Course> courses = new ArrayList<>();
        courses.add(new Course("Matemáticas", 90.0));
        courses.add(new Course("Ciencias", 80.0));
        courses.add(new Course("Inglés", 85.0));

        double average = calculator.calculateAverage(courses);

        assertEquals((90.0 + 80.0 + 85.0) / 3, average);
    }
}
