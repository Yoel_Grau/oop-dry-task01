import java.util.List;

public interface GradeCalculator {
    double calculateAverage(List<Course> courses);
}

