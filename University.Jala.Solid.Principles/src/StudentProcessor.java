import java.util.List;

public class StudentProcessor {
    private InfoPrinter printer;

    public StudentProcessor(InfoPrinter printer) {
        this.printer = printer;
    }

    public void process(List<Student> students) {
        for (Student student : students) {
            printer.print(student);
        }
    }
}
