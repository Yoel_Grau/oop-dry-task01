import java.util.ArrayList;
import java.util.List;

public class Student {
    private int id;
    private String name;
    private List<Course> Courses;
    private GradeCalculator gradeCalculator;

    public Student(int id, String name, GradeCalculator gradeCalculator) {
        this.id = id;
        this.name = name;
        this.Courses = new ArrayList<>();
        this.gradeCalculator = gradeCalculator;
    }

    public void addSubject(Course subject) {
        Courses.add(subject);
    }

    public double calculateAverage() {
        return gradeCalculator.calculateAverage(Courses);
    }

    public String getName() { return name; }
    public int getId() { return id; }
    public List<Course> getSubjects() { return Courses; }
}


