import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        GradeCalculator gradeCalculator = new SimpleGradeCalculator();

        List<Student> students = new ArrayList<>();

        students.add(new Student(1022543764, "Gery", gradeCalculator));
        students.add(new Student(1045673987, "Joel", gradeCalculator));
        students.add(new Student(1046876234, "Carmen", gradeCalculator));
        students.add(new Student(1055325876, "Lia", gradeCalculator));
        students.add(new Student(1057634984, "Phill", gradeCalculator));

        students.get(0).addSubject(new Course("Ciencias", 67.0));
        students.get(0).addSubject(new Course("Matemáticas", 70.0));
        students.get(0).addSubject(new Course("Inglés", 69.0));

        students.get(1).addSubject(new Course("Ciencias", 80.0));
        students.get(1).addSubject(new Course("Matemáticas", 85.0));
        students.get(1).addSubject(new Course("Inglés", 85.0));

        students.get(2).addSubject(new Course("Ciencias", 90.5));
        students.get(2).addSubject(new Course("Matemáticas", 65.5));
        students.get(2).addSubject(new Course("Inglés", 95.5));

        students.get(3).addSubject(new Course("Ciencias", 75.3));
        students.get(3).addSubject(new Course("Matemáticas", 75.3));
        students.get(3).addSubject(new Course("Inglés", 85.3));

        students.get(4).addSubject(new Course("Ciencias", 70.0));
        students.get(4).addSubject(new Course("Matemáticas", 80.0));
        students.get(4).addSubject(new Course("Inglés", 74.0));

        StudentProcessor processor = new StudentProcessor(new StudentsInfoPrinter());

        processor.process(students);
    }
}




