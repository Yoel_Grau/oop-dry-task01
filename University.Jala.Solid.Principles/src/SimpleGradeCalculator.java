import java.util.List;

public class SimpleGradeCalculator implements GradeCalculator {
    @Override
    public double calculateAverage(List<Course> courses) {
        double total = 0;
        for (Course subject : courses) {
            total += subject.getScore();
        }
        return courses.isEmpty() ? 0 : total / courses.size();
    }
}

