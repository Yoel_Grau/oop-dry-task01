public class StudentsInfoPrinter implements InfoPrinter {
    @Override
    public void print(Student student) {
        System.out.println("Estudiante: " + student.getName() + ", ID: " + student.getId());
        for (Course subject : student.getSubjects()) {
            System.out.println("Materia: " + subject.getName() + ", Calificación: " + subject.getScore());
        }
        String formattedAverage = String.format("%.2f", student.calculateAverage());
        System.out.println("Nota promedio: " + formattedAverage);
        System.out.println();
    }
}

